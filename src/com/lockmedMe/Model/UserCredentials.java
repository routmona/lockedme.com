package com.lockmedMe.Model;

public class UserCredentials {

private String sitename;
private String loggedInUser;
private String username;
private String password;

public UserCredentials() {};

public UserCredentials(String sitename, String loggedInUser, String username, String password)
{
	this.sitename = sitename;
	this.loggedInUser= loggedInUser;
	this.username = username;
	this.password=password;
}

public String getSitename()
{
	return sitename;
}

public void setsitename(String sitename)
{
	this.sitename = sitename;
}

public String getloggedInUser()
{
	return loggedInUser;
	
}

public void setloggedInUser(String loggedInUser)
{
	this.loggedInUser = loggedInUser;
}

public String getUsername()
{
	return username;
}
public void setUsername(String username)
{
	this.username = username;
}
public String getPassword()
{
	return password;
}
public void setPassword(String password)
{
	this.password=password;
}

@Override
public String toString()
{
	return "UserCredentials [siteName=" + sitename +
			", loggedInUser=" + loggedInUser +
			", username=" + username
			+ ", password=" + password + "]";
}
}
