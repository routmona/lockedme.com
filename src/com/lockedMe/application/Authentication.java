package com.lockedMe.application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Scanner;

import com.lockmedMe.Model.UserCredentials;
import com.lockmedMe.Model.Users;

public class Authentication {
	// input data
	private static Scanner keyboard;
	private static Scanner input;
	// output data
	private static PrintWriter output;
	private static PrintWriter lockerOutput;

	// model to store data
	private static Users users;
	private static UserCredentials userCredentials;

	public static void main(String[] args) {
		initApp();
		welcomeScreen();
		signInOptions();
	}

	public static void signInOptions() {
		System.out.println("1. Registration ");
		System.out.println("2.  Login ");
		int option = keyboard.nextInt();
		switch (option) {
		case 1:
			registerUser();
			break;
		case 2:
			loginUser();
			break;
		default:
			System.out.println("please select 1 or 2");
			break;
		}
		keyboard.close();
		input.close();
	}

	public static void registerUser() {
		System.out.println("===================================");
		System.out.println("*                    *");
		System.out.println("*   WELCOME TO REGISTRATION PAGE");
		System.out.println("*                    *");
		System.out.println("===================================");

		System.out.println("Enter Username :");
		String username = keyboard.next();
		users.setUsername(username);

		System.out.println("Enter password");
		String password = keyboard.next();
		users.setPassword(password);

		output.println(users.getUsername());
		output.println(users.getPassword());

		System.out.println("User registration is successful");
		output.close();
	}

	public static void loginUser() {
		System.out.println("=================================");
		System.out.println("*                        *");
		System.out.println("*    WELCOME TO LOGIN PAGE    *");
		System.out.println("*                         *");
		System.out.println("============================");
		System.out.println("Enter Username :");
		String inpUsername = keyboard.next();
		boolean found = false;
		while (input.hasNext() && !found) {
			if (input.next().equals(inpUsername)) {
				System.out.println("Enter password :");
				String inpPassword = keyboard.next();
				if (input.next().equals(inpPassword)) {
					System.out.println("Login is successful !");
					found = true;
					System.out.println("to which account you want to login : 1.Facebook \n"
							+ "2.Instagram \n"
							+ "3.Twitter \n");
					String inpAccount = keyboard.next();
					System.out.println("Enter your password :");
					String inpPassword2 = keyboard.next();
					System.out.println("welcome to your account");
					break;
				}
			}
		}
		if (!found) {
			System.out.println("User not found : Login failure");
		}
	}

	public static void welcomeScreen() {
		System.out.println("===========================");
		System.out.println("*                      *");
		System.out.println("*      Welcome to LockMe.com       *");
		System.out.println("*      Your personal digital locker     *");
		System.out.println("*                      *");
		System.out.println("============================");

	}

	public static void initApp() {
		File dbFile = new File("C:\\Users\\monalisha_rout\\eclipse\\jee-2019-12\\JavaProject\\src\\com\\lockedMe\\application\\database.txt");
		File lockerFile = new File("C:\\Users\\monalisha_rout\\eclipse\\jee-2019-12\\JavaProject\\src\\com\\lockedMe\\application\\locker-file.txt");

		try {
			// read data from file
			input = new Scanner(dbFile);
			// read data from keyboard
			keyboard = new Scanner(System.in);

			// output
			output = new PrintWriter(new FileWriter(dbFile, true));
			lockerOutput = new PrintWriter(new FileWriter(lockerFile, true));

			users = new Users();
			userCredentials = new UserCredentials();

		} catch (IOException e) {
			System.out.println("File not Found");

		}

	}

}
